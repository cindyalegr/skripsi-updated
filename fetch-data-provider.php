<?php
include "connection.php";

if(isset($_POST["action"]))
{
    $query = "SELECT produk.*, kota.*, provinsi.* FROM produk
     INNER JOIN kota ON kota.id_kota = produk.id_kota
     INNER join provinsi ON provinsi.id_provinsi = kota.id_provinsi 
    WHERE id_provider ='".$_POST["provider"]."'
    ";

    if(isset($_POST["keyword"]) && $_POST["keyword"] != ''){
        $query .= "AND nama_produk LIKE '%".$_POST["keyword"]."%'";
    }
    if(isset($_POST["city"]) && $_POST["city"] != ''){
        $query .= " AND id_kota = ".$_POST["city"]." ";
    }
    if(isset($_POST["urut"])){
        if($_POST["urut"]=="newest"){
            $query .= " ORDER BY tanggal_awal DESC";
        }
        elseif ($_POST["urut"]=="oldest"){
            $query .= " ORDER BY tanggal_awal ASC";
        }
        elseif ($_POST["urut"]=="cheapest"){
            $query .= " ORDER BY harga ASC";
        }
        elseif ($_POST["urut"]=="most_expensive"){
            $query .= " ORDER BY harga DESC";
        }
        elseif ($_POST["urut"]=="name_asc"){
            $query .= " ORDER BY nama_produk ASC";
        }
        elseif ($_POST["urut"]=="name_desc"){
            $query .= " ORDER BY nama_produk DESSC";
        }
    }


    $getQuery = mysqli_query($con, $query);

    $total_row = mysqli_num_rows($getQuery);
    $output = '';


    if($total_row > 0)
    {
        while ($row = mysqli_fetch_assoc($getQuery))
        {
            $harga = number_format($row["harga"]);

            $output .= '
            <div class="col-lg-4 col-md-6 col-product">
                <a href="detilproduk.php?id='.$row["id_produk"].'">
                    <div class="card">
                        <img class="card-img-top" src="Images/'.$row["thumbnail"].'" alt="Card image cap">
                        <div class="card-body">
                            <div class="box-product-tags py-3">
                            </div>
                            <h3>'.$row["nama_produk"].'</h3>
                            <p class="card-text">'.$row["deskripsi"].'</p>
                            <table class="table-product">
                                <tbody>
                                <tr>
                                    <td>
                                        <img src="assets/pin.png" alt="">
                                    </td>
                                    <td colspan="3">'.$row["nama_kota"].', '.$row["nama_provinsi"].'</td>
                                </tr>
                                <tr>
                                    <td>
                                        <img src="assets/wall-clock.png" alt="">
                                    </td>
                                    <td colspan="3">'.$row["durasi"].'</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="bg-light-blue text-center card-footer">
                            <p>Mulai Dari</p>
                            <p class="bold fs-22">IDR'.$harga.'</p>
                            <button class="btn btn-primary">Pesan Sekarang</button>
                        </div>
                    </div>
                </a>
            </div>
            ';
        }
    }
    else
    {
        $output = '<h3>No Data Found</h3>';
    }
    echo $output;
}
?>