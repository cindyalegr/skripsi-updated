<?php
include "connection.php";
?>

<?php
$sqlProduk = "SELECT * FROM produk WHERE id_produk=".$_GET['id'];
$queryProduk = mysqli_query($con, $sqlProduk);
$getProduk = mysqli_fetch_assoc($queryProduk);

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <title><?php echo $getProduk['nama_produk'];?></title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    <link rel="stylesheet" href="http://majumuju.kadalmacho.top/landing/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
    <link href="http://majumuju.kadalmacho.top/css/material.css" rel="stylesheet">
    <link rel="stylesheet" href="http://majumuju.kadalmacho.top/css/app.css">
    <link rel="shortcut icon" href="Images/logo1.png">
    <link rel="stylesheet" href="http://majumuju.kadalmacho.top/css/badge-company.css">
    <link rel="stylesheet" href="http://majumuju.kadalmacho.top/css/badge-discount.css">
    <link rel="stylesheet" href="http://majumuju.kadalmacho.top/css/customer-inline-fix.css">
    <style>
        .card-header .btn-link {
            text-decoration: none;
            width: 100%;
        }
        @media (min-width: 320px) and (max-width: 991px){
            #payment-virtual-account .card-body a.backHome {
                font-size: 9px;
            }
            #payment-virtual-account .col-12 {
                padding-right: 5px;
                padding-left: 5px;
            }
            #payment-virtual-account .card-payment ol {
                padding-left: 10px;
            }
            #payment-virtual-account .bank-list {
                padding-left: 10px;
                padding-right: 10px;
            }
        }
    </style>

    <style>
        .address {
            color: #fff;
            padding-bottom: 0.8rem;
            text-align: justify;
        }

        .address p {
            margin-bottom: 0px;
        }

        footer .powered {
            height: auto;
        }

        footer .phone-company span {
            display: inline-flex;
        }

        footer .phone-company {
            margin-top: 1rem;
        }

        footer .phone-company img {
            width: 20px;
        }

        footer p.about, footer .address #address, footer .social {
            font-weight: bold;
            margin-bottom: 0.6rem;
            margin-top: 0;
        }

        footer .address #address {
            margin-bottom: 0.95rem;
        }

        footer .pt-5 {
            padding-top: 2.5rem !important;
        }

        nav.navbar + section {
            margin-top: 49px !important;
        }

        #btn-navbar-toggle {
            cursor: pointer;
            background: none;
            border: none;
            display: none;
        }

        .relative img {
            width: 1.3rem;
        }

        .fa.fa-bars {
            color: #2699FB;
            font-size: 1.5rem;
        }

        .owl-nav .owl-next.disabled, .owl-nav .owl-prev.disabled {
            display: none
        }

        @media  only screen and (max-width: 480px) {
            #btn-navbar-toggle {
                display: block;
            }

            .collapse.navbar-collapse {
                background: #f1f8fd;
                margin-left: -1rem;
                margin-right: -1rem;
                border-top: 1px solid #dcdbdb;
            }

            footer .address #address {
                margin-top: 35px;
            }
        }
    </style>

</head>
<body>
<div class="loading">
    <div class="loading-content">
        <div class="spin">
            <i class="fa fa-circle-o-notch fa-spin"></i>
        </div>
        <div class="loading-text">
            Memuat..
        </div>
    </div>
</div>

<!--PHP DATA LOAD-->
<?php
$sqlProvider = "SELECT * FROM provider WHERE id_provider=".$getProduk['id_provider'];
$queryProvider = mysqli_query($con, $sqlProvider);
$getProvider = mysqli_fetch_assoc($queryProvider);

?>

<nav class="navbar navbar-dark navbar-expand-md bg-style-1 container-fluid navbar-fixed-top" id="topNavbar">
    <div class="container">
        <a href="provider.php?id=<?php echo $getProduk['id_provider'];?>" class="navbar-brand">
            <img src="Images/logo1.png" alt="">
        </a>
        <div class="profile-info">
            <h3 id="company-name"><?php echo $getProvider['nama_provider'];?></h3>
        </div>
        <span class="relative"></span>
        <button id="btn-navbar-toggle" data-target="#navcol-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="fa fa-bars"></span>
        </button>
        <div class="collapse navbar-collapse" id="navcol-1">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item text-center" id="landing-language" role="presentation">
                    <div class="current-language">
                        <div class="description-current-language">
                            Indonesia
                        </div>
                        <div class="flag-current-language">
                            <img class="img-circle" src="http://majumuju.kadalmacho.top/landing/img/idn.png" alt="">
                        </div>
                        <div class="caret-language">
                            <i class="fa fa-caret-down"></i>
                        </div>
                    </div>
                    <div class="language-option" style="display: none;">
                        <ul>
                            <li data-value="en" class="pick-lang">
                                <div class="box-language">
                                    <div class="description-language">
                                        English
                                    </div>
                                    <div class="flag-language">
                                        <img src="http://majumuju.kadalmacho.top/landing/img/uk.png" alt="">
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <form action="#" method="POST">
                        <input type="hidden" name="_token" value="ezWB0H9vHdiimVMEPYtH1aMu906hbkUCAQxgftr5">
                        <input type="hidden" name="lang">
                    </form>
                </li>
            </ul>
        </div>
    </div>
</nav>

<section id="content">
    <div class="bg-light-blue block-height">
        <div class="container pt-5">
            <ul class="breadcrumb">
                <li><a href="provider.php?id=<?php echo $getProduk['id_provider'];?>">Beranda</a></li>
                <li><a href="detilproduk.php?id=<?php echo $getProduk['id_produk'];?>"><?php echo $getProduk['nama_produk'];?></a>
                </li>
                <li><a>Invoice</a></li>
            </ul>
        </div>
        <div id="payment-virtual-account" class="container pb-5">
            <div class="row">
                <div class="col-12 py-3 text-center">
                    <p class="fs-14">Sisa waktu pembayaran :</p>
                </div>
                <div class="col-12 text-center">
                    <div class="count-down">
                        <div class="number days">
                            00
                        </div>
                        <div class="desc">
                            Hari
                        </div>
                    </div>
                    <div class="count-down">
                        <div class="number hours">
                            00
                        </div>
                        <div class="desc">
                            Jam
                        </div>
                    </div>
                    <div class="count-down">
                        <div class="number minutes">
                            00
                        </div>
                        <div class="desc">
                            Menit
                        </div>
                    </div>
                    <div class="count-down">
                        <div class="number seconds">
                            00
                        </div>
                        <div class="desc">
                            Detik
                        </div>
                    </div>
                </div>

                <div class="col-12 text-center py-3">
                    <h2 class="bold">IDR
                        <?php
                            $banyak = $_POST["pax"];
                            $harga_dasar = $getProduk["harga"];
                            $total = $banyak * $harga_dasar;
                        echo $total;
                        ?>
                    </h2>
                    <div class="can-copy">
                        INVOICE <span class="bold data-copied">INV200914513867</span> | <span
                            class="bold"><?php echo $_POST["email"]?></span>
                    </div>
                    <div class="bold">
                        <?php echo $getProduk['nama_produk'];?>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                    <div class="row py-3">
                        <div class="col-md-6 order-1 order-md-0 text-center text-md-left">
                            <span class="caption">Pilih Metode Pembayaran</span>
                        </div>
                        <div class="col-md-6 text-md-right order-0 order-md-1  mb-lg-0 mb-3 text-center">
                            <a href="provider.php?id=<?php echo $getProduk['id_provider'];?>"
                               class="backHome embed-remove">Kembali ke <?php echo $getProvider['nama_provider'];?></a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="accordion" id="accordionExample">
                                <div class="card mt-1 mb-1">
                                    <div class="card-header" id="headingOneMANDIRI">
                                        <h5 class="btn btn-link bold" type="button" data-toggle="collapse"
                                            data-target="#collapseOneMANDIRI"
                                            aria-expanded="true"
                                            aria-controls="collapseOneMANDIRI">
                                            MANDIRI
                                        </h5>
                                    </div>
                                    <div id="collapseOneMANDIRI" class="collapse"
                                         aria-labelledby="headingOneMANDIRI"
                                         data-parent="#accordionExample">
                                        <div class="card-body bank-list">
                                            <div class="col-12 text-center virtual-account can-copy">
                                                <h3>Virtual Account # <span
                                                        class="data-copied">8860893157772</span>
                                                </h3>
                                                <h3>Nama Virtual Akun
                                                    # GOMODO</h3>
                                            </div>
                                            <div class="col-12">
                                                <div class="accordion" id="accordionMenuMandiri">
                                                    <div class="card card-payment mt-1 mb-1">
                                                        <div class="card-header" id="headingTwoATMMandiri">
                                                            <h5 class="btn btn-link" type="button"
                                                                data-toggle="collapse"
                                                                data-target="#collapseTwoATMMandiri"
                                                                aria-expanded="true"
                                                                aria-controls="collapseTwoATMMandiri">
                                                                ATM
                                                            </h5>
                                                        </div>
                                                        <div id="collapseTwoATMMandiri" class="collapse"
                                                             aria-labelledby="headingTwoATMMandiri"
                                                             data-parent="#accordionMenuMandiri">
                                                            <div class="card-body">
                                                                <div class="col-12">
                                                                    <ol>
                                                                        <li>Masukkan kartu ATM dan pilih "Bahasa Indonesia"</li>
                                                                        <li>Ketik nomor PIN kartu ATM</li>
                                                                        <li>Pilih menu BAYAR/BELI, kemudian pilih menu MULTI PAYMENT</li>
                                                                        <li>Ketik kode perusahaan, yaitu "88908" (88908 XENDIT), tekan "BENAR"</li>
                                                                        <li>Masukkan nomor Virtual Account
                                                                            <span class="font-weight-bold">8860893157772</span>,  tekan "BENAR"
                                                                        </li>
                                                                        <li>Isi NOMINAL, kemudian tekan "BENAR"</li>
                                                                        <li>Muncul konfirmasi data customer. Pilih Nomor 1 sesuai tagihan yang akan dibayar, kemudian tekan "YA"</li>
                                                                        <li>Muncul konfirmasi pembayaran. Tekan "YA" untuk melakukan pembayaran</li>
                                                                        <li>Bukti pembayaran dalam bentuk struk agar disimpan sebagai bukti pembayaran yang sah dari Bank Mandiri</li>
                                                                        <li>Transaksi Anda sudah selesai</li>
                                                                        <li>Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</li>
                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card card-payment mt-1 mb-1">
                                                        <div class="card-header"
                                                             id="headingTwoMobilebankingMandiri">
                                                            <h5 class="btn btn-link" type="button"
                                                                data-toggle="collapse"
                                                                data-target="#collapseTwoMobileBankingMandiri"
                                                                aria-expanded="true"
                                                                aria-controls="collapseTwoMobileBankingMandiri">
                                                                IBanking Mandiri
                                                            </h5>
                                                        </div>
                                                        <div id="collapseTwoMobileBankingMandiri"
                                                             class="collapse"
                                                             aria-labelledby="headingTwoMobilebankingMandiri"
                                                             data-parent="#accordionMenuMandiri">
                                                            <div class="card-body">
                                                                <div class="col-12">
                                                                    <ol>
                                                                        <li>Kunjungi website Mandiri Internet Banking dengan alamat
                                                                            <a href="https://ib.bankmandiri.co.id">https://ib.bankmandiri.co.id</a>
                                                                        </li>
                                                                        <li>Login dengan memasukkan USER ID dan PIN</li>
                                                                        <li>Masuk ke halaman Beranda, lalu pilih "Bayar"</li>
                                                                        <li>Pilih "Multi Payment"</li>
                                                                        <li>Pilih "No Rekening Anda"</li>
                                                                        <li>Pilih Penyedia Jasa "88908 XENDIT"</li>
                                                                        <li>Pilih "Nomor Virtual Account"</li>
                                                                        <li>Masukkan nomor Virtual Account
                                                                            <span class="font-weight-bold">8860893157772</span>
                                                                        </li>
                                                                        <li>Masuk ke halaman konfirmasi 1</li>
                                                                        <li>Apabila benar/sesuai, klik tombol tagihan TOTAL, kemudian klik "Lanjutkan"</li>
                                                                        <li>Masuk ke halaman konfirmasi 2</li>
                                                                        <li>Masuk Challenge Code yang dikirimkan ke Token Internet Banking Anda, kemudian klik "Kirim"</li>
                                                                        <li>Masuk ke halaman konfirmasi pembayaran telah selesai</li>
                                                                        <li>Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</li>

                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card card-payment mt-1 mb-1">
                                                        <div class="card-header"
                                                             id="headingTwoIbankPersonalMandiri">
                                                            <h5 class="btn btn-link" type="button"
                                                                data-toggle="collapse"
                                                                data-target="#collapseTwoIbankPersonalMandiri"
                                                                aria-expanded="true"
                                                                aria-controls="collapseTwoIbankPersonalMandiri">
                                                                MBanking Mandiri
                                                            </h5>
                                                        </div>
                                                        <div id="collapseTwoIbankPersonalMandiri"
                                                             class="collapse"
                                                             aria-labelledby="headingTwoIbankPersonalMandiri"
                                                             data-parent="#accordionMenuMandiri">
                                                            <div class="card-body">
                                                                <div class="col-12">
                                                                    <ol>
                                                                        <li>Buka M-Banking Mandiri dan masukkan PIN</li>
                                                                        <li>Pilih menu "Bayar"</li>
                                                                        <li>Pilih "Buat Pembayaran Baru"</li>
                                                                        <li>Pilih "Multi Payment"</li>
                                                                        <li>Pilih Penyedia Jasa "88908 XENDIT"</li>
                                                                        <li>Masukkan rekening tujuan
                                                                            <span class="font-weight-bold">8860893157772</span> yang hendak dibayarkan
                                                                        </li>
                                                                        <li>Masukkan Nominal transfer</li>
                                                                        <li>Beri keterangan bila perlu, kemudian tekan "LANJUT"</li>
                                                                        <li>Masukkan MPIN Anda untuk menyelesaikan transaksi</li>
                                                                        <li>Unduh bukti transfer sebagai bukti pembayaran Anda yang sah</li>
                                                                        <li>Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</li>
                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mt-1 mb-1">
                                    <div class="card-header" id="headingOneBRI">
                                        <h5 class="btn btn-link bold" type="button" data-toggle="collapse"
                                            data-target="#collapseOneBRI"
                                            aria-expanded="true"
                                            aria-controls="collapseOneBRI">
                                            BRI
                                        </h5>
                                    </div>
                                    <div id="collapseOneBRI" class="collapse"
                                         aria-labelledby="headingOneBRI"
                                         data-parent="#accordionExample">
                                        <div class="card-body bank-list">
                                            <div class="col-12 text-center virtual-account can-copy">
                                                <h3>Virtual Account # <span
                                                        class="data-copied">2621540982268</span>
                                                </h3>
                                                <h3>Nama Virtual Akun
                                                    # GOMODO</h3>
                                            </div>
                                            <div class="col-12">
                                                <div class="accordion" id="accordionMenuBRI">
                                                    <div class="card card-payment mt-1 mb-1">
                                                        <div class="card-header" id="headingTwoATMBRI">
                                                            <h5 class="btn btn-link" type="button"
                                                                data-toggle="collapse"
                                                                data-target="#collapseTwoATMBRI"
                                                                aria-expanded="true"
                                                                aria-controls="collapseTwoATMBRI">
                                                                ATM BRI
                                                            </h5>
                                                        </div>
                                                        <div id="collapseTwoATMBRI" class="collapse"
                                                             aria-labelledby="headingTwoATMBRI"
                                                             data-parent="#accordionMenuBRI">
                                                            <div class="card-body">
                                                                <div class="col-12">
                                                                    <ol>
                                                                        <li>Masukkan kartu, pilih bahasa kemudian masukkan PIN Anda</li>
                                                                        <li>Pilih "Transaksi Lain" lalu pilih "Pembayaran"</li>
                                                                        <li>Pilih "Lainnya" lalu pilih "Briva"</li>
                                                                        <li>Masukkan nomor Virtual Account
                                                                            <span class="font-weight-bold">2621540982268</span> dan nominal yang ingin Anda bayar
                                                                        </li>
                                                                        <li>Periksa kembali data transaksi kemudian tekan "YA"</li>
                                                                        <li>Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</li>
                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card card-payment mt-1 mb-1">
                                                        <div class="card-header"
                                                             id="headingTwoMobilebankingBRI">
                                                            <h5 class="btn btn-link" type="button"
                                                                data-toggle="collapse"
                                                                data-target="#collapseTwoMobileBankingBRI"
                                                                aria-expanded="true"
                                                                aria-controls="collapseTwoMobileBankingBRI">
                                                                IBanking BRI
                                                            </h5>
                                                        </div>
                                                        <div id="collapseTwoMobileBankingBRI" class="collapse"
                                                             aria-labelledby="headingTwoMobilebankingBRI"
                                                             data-parent="#accordionMenuBRI">
                                                            <div class="card-body">
                                                                <div class="col-12">
                                                                    <ol>
                                                                        <li>Login di
                                                                            <a href="https://ib.bri.co.id">https://ib.bri.co.id</a> masukkan USER ID dan PASSWORD
                                                                        </li>
                                                                        <li>Pilih "Pembayaran" lalu pilih "Briva"</li>
                                                                        <li>Masukkan nomor Virtual Account Anda
                                                                            <span class="font-weight-bold">2621540982268</span>, serta nominal yang akan dibayar, lalu klik kirim
                                                                        </li>
                                                                        <li>Masukkan kembali PASSWORD Anda serta kode otentikasi mToken internet banking</li>
                                                                        <li>Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</li>
                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card card-payment mt-1 mb-1">
                                                        <div class="card-header"
                                                             id="headingTwoIbankPersonalBRI">
                                                            <h5 class="btn btn-link" type="button"
                                                                data-toggle="collapse"
                                                                data-target="#collapseTwoIbankPersonalBRI"
                                                                aria-expanded="true"
                                                                aria-controls="collapseTwoIbankPersonalBRI">
                                                                MBanking BRI
                                                            </h5>
                                                        </div>
                                                        <div id="collapseTwoIbankPersonalBRI"
                                                             class="collapse"
                                                             aria-labelledby="headingTwoIbankPersonalBRI"
                                                             data-parent="#accordionMenuBRI">
                                                            <div class="card-body">
                                                                <div class="col-12">
                                                                    <ol>
                                                                        <li>Login ke BRI Mobile Banking, masukkan USER ID dan PIN Anda</li>
                                                                        <li>Pilih "Pembayaran" lalu pilih "Briva"</li>
                                                                        <li>Masukkan nomor Virtual Account Anda
                                                                            <span class="font-weight-bold">2621540982268</span>, serta nominal yang akan dibayar, lalu klik kirim
                                                                        </li>
                                                                        <li>Masukkan nomor PIN anda dan klik "Kirim"</li>
                                                                        <li>Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</li>
                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mt-1 mb-1">
                                    <div class="card-header" id="headingOneBNI">
                                        <h5 class="btn btn-link bold" type="button" data-toggle="collapse"
                                            data-target="#collapseOneBNI"
                                            aria-expanded="true"
                                            aria-controls="collapseOneBNI">
                                            BNI
                                        </h5>
                                    </div>
                                    <div id="collapseOneBNI" class="collapse"
                                         aria-labelledby="headingOneBNI"
                                         data-parent="#accordionExample">
                                        <div class="card-body bank-list">
                                            <div class="col-12 text-center virtual-account can-copy">
                                                <h3>Virtual Account # <span
                                                        class="data-copied">880881861856</span>
                                                </h3>
                                                <h3>Nama Virtual Akun
                                                    # GOMODO</h3>
                                            </div>
                                            <div class="col-12">
                                                <div class="accordion" id="accordionMenu">
                                                    <div class="card card-payment mt-1 mb-1">
                                                        <div class="card-header" id="headingTwoATM">
                                                            <h5 class="btn btn-link" type="button"
                                                                data-toggle="collapse"
                                                                data-target="#collapseTwoATM"
                                                                aria-expanded="true"
                                                                aria-controls="collapseTwoATM">
                                                                ATM
                                                            </h5>
                                                        </div>
                                                        <div id="collapseTwoATM" class="collapse"
                                                             aria-labelledby="headingTwoATM"
                                                             data-parent="#accordionMenu">
                                                            <div class="card-body">
                                                                <div class="col-12">
                                                                    <ol>
                                                                        <li>Masukkan kartu Anda</li>
                                                                        <li>Pilih Bahasa</li>
                                                                        <li>Masukkan PIN ATM Anda</li>
                                                                        <li>Pilih "Menu Lainnya"</li>
                                                                        <li>Pilih Transfer</li>
                                                                        <li>Pilih jenis rekening yang akan Anda gunakan (Contoh: "Dari Rekening Tabungan")</li>
                                                                        <li>Pilih "Virtual Account Billing"</li>
                                                                        <li>Masukkan nomor Virtual Account yang akan dituju
                                                                            <span class="font-weight-bold">880881861856</span>
                                                                        </li>
                                                                        <li>Tagihan yang harus dibayarkan akan muncul pada layar konfirmasi</li>
                                                                        <li>Konfirmasi, apabila telah sesuai, lanjutkan transaksi</li>
                                                                        <li>Transaksi Anda telah selesai</li>
                                                                        <li>Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</li>
                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card card-payment mt-1 mb-1">
                                                        <div class="card-header" id="headingTwoMobilebanking">
                                                            <h5 class="btn btn-link" type="button"
                                                                data-toggle="collapse"
                                                                data-target="#collapseTwoMobileBanking"
                                                                aria-expanded="true"
                                                                aria-controls="collapseTwoMobileBanking">
                                                                Mobile Banking BNI
                                                            </h5>
                                                        </div>
                                                        <div id="collapseTwoMobileBanking" class="collapse"
                                                             aria-labelledby="headingTwoMobilebanking"
                                                             data-parent="#accordionMenu">
                                                            <div class="card-body">
                                                                <div class="col-12">
                                                                    <ol>
                                                                        <li>Masukkan User ID dan Password</li>
                                                                        <li>Pilih menu "Transfer"</li>
                                                                        <li>Pilih menu "Virtual Account Billing" kemudian pilih rekening debet</li>
                                                                        <li>Masukkan nomor Virtual Account yang akan dituju
                                                                            <span class="font-weight-bold">880881861856</span> pada menu "Input Baru"
                                                                        </li>
                                                                        <li>Tagihan yang harus dibayarkan akan muncul pada layar konfirmasi</li>
                                                                        <li>Konfirmasi transaksi dan masukkan Password Transaksi</li>
                                                                        <li>Pembayaran Anda telah berhasil</li>
                                                                        <li>Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</li>

                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card card-payment mt-1 mb-1">
                                                        <div class="card-header" id="headingTwoIbankPersonal">
                                                            <h5 class="btn btn-link" type="button"
                                                                data-toggle="collapse"
                                                                data-target="#collapseTwoIbankPersonal"
                                                                aria-expanded="true"
                                                                aria-controls="collapseTwoIbankPersonal">
                                                                IBank Personal BNI
                                                            </h5>
                                                        </div>
                                                        <div id="collapseTwoIbankPersonal" class="collapse"
                                                             aria-labelledby="headingTwoIbankPersonal"
                                                             data-parent="#accordionMenu">
                                                            <div class="card-body">
                                                                <div class="col-12">
                                                                    <ol>
                                                                        <li>Ketik alamat
                                                                            <a href="https://ibank.bni.co.id">https://ibank.bni.co.id</a> kemudian klik "Enter"
                                                                        </li>
                                                                        <li>Masukkan User ID dan password</li>
                                                                        <li>Pilih menu "Transfer"</li>
                                                                        <li>Pilih menu "Virtual Account Billing"</li>
                                                                        <li>Kemudian masukan nomor Virtual Account
                                                                            <span class="font-weight-bold">880881861856</span> yang hendak dibayarkan
                                                                        </li>
                                                                        <li>Pilih rekening debet yang akan digunakan, kemudian tekan "Lanjut"</li>
                                                                        <li>Kemudian tagihan yang harus dibayarkan akan muncul pada layar konfirmasi</li>
                                                                        <li>Masukkan Kode Otentikasi Token</li>
                                                                        <li>Pembayaran Anda telah berhasil</li>
                                                                        <li>Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</li>
                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card card-payment mt-1 mb-1">
                                                        <div class="card-header" id="headingTwoSMSbanking">
                                                            <h5 class="btn btn-link" type="button"
                                                                data-toggle="collapse"
                                                                data-target="#collapseTwoSMSbanking"
                                                                aria-expanded="true"
                                                                aria-controls="collapseTwoSMSbanking">
                                                                SMS Banking
                                                            </h5>
                                                        </div>
                                                        <div id="collapseTwoSMSbanking" class="collapse"
                                                             aria-labelledby="headingTwoSMSbanking"
                                                             data-parent="#accordionMenu">
                                                            <div class="card-body">
                                                                <div class="col-12">
                                                                    <ol>
                                                                        <li>Buka aplikasi SMS Banking BNI</li>
                                                                        <li>Pilih menu "Transfer"</li>
                                                                        <li>Pilih menu "Transfer Rekening BNI"</li>
                                                                        <li>Masukkan nomor Virtual Account yang akan dituju
                                                                            <span class="font-weight-bold">880881861856</span>
                                                                        </li>
                                                                        <li>Masukkan nominal transfer sesuai dengan tagihan atau kewajiban Anda. Nominal yang berbeda tidak dapat diproses</li>
                                                                        <li>Pilih "Proses" kemudian "Setuju"</li>
                                                                        <li>Reply sms dengan ketik pin sesuai perintah</li>
                                                                        <li>Transaksi berhasil</li>
                                                                        <li>Atau dapat juga langsung mengetik sms dengan format: TRF[SPASI]NomorVA[SPASI]NOMINAL dan kemudian kirim ke 3346</li>
                                                                        <li>Contoh: TRF  880881861856 300000</li>
                                                                        <li>Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</li>
                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card card-payment mt-1 mb-1">
                                                        <div class="card-header" id="headingTwoTellerBNI">
                                                            <h5 class="btn btn-link" type="button"
                                                                data-toggle="collapse"
                                                                data-target="#collapseTwoTellerBNI"
                                                                aria-expanded="true"
                                                                aria-controls="collapseTwoTellerBNI">
                                                                Teller BNI
                                                            </h5>
                                                        </div>
                                                        <div id="collapseTwoTellerBNI" class="collapse"
                                                             aria-labelledby="headingTwoTellerBNI"
                                                             data-parent="#accordionMenu">
                                                            <div class="card-body">
                                                                <div class="col-12">
                                                                    <ol>
                                                                        <li>Kunjungi Kantor Cabang atau Outlet BNI terdekat</li>
                                                                        <li>Informasikan kepada Teller, bahwa ingin melakukan pembayaran "Virtual Account Billing"</li>
                                                                        <li>Serahkan nomor Virtual Account yang akan dituju
                                                                            <span class="font-weight-bold">880881861856</span> kepada Teller
                                                                        </li>
                                                                        <li>Teller melakukan konfirmasi kepada Anda</li>
                                                                        <li>Teller memproses transaksi</li>
                                                                        <li>Jika transaksi berhasil, Anda akan menerima bukti pembayaran dari teller</li>
                                                                        <li>Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</li>
                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card card-payment mt-1 mb-1">
                                                        <div class="card-header" id="headingTwoAgen46">
                                                            <h5 class="btn btn-link" type="button"
                                                                data-toggle="collapse"
                                                                data-target="#collapseTwoAgen46"
                                                                aria-expanded="true"
                                                                aria-controls="collapseTwoAgen46">
                                                                Agen 46
                                                            </h5>
                                                        </div>
                                                        <div id="collapseTwoAgen46" class="collapse"
                                                             aria-labelledby="headingTwoAgen46"
                                                             data-parent="#accordionMenu">
                                                            <div class="card-body">
                                                                <div class="col-12">
                                                                    <ol>
                                                                        <li>Kunjungi Agen46 terdekat (warung, toko, atau kios dengan tulisan Agen46)</li>
                                                                        <li>Informasikan kepada Agen46, bahwa ingin melakukan pembayaran "Virtual Account"</li>
                                                                        <li>Serahkan nomor Virtual Account yang akan dituju
                                                                            <span class="font-weight-bold">880881861856</span> kepada Agen46
                                                                        </li>
                                                                        <li>Agen46 melakukan konfirmasi kepada Anda</li>
                                                                        <li>Agen46 memproses transaksi</li>
                                                                        <li>Apabila transaksi sukses anda akan menerima bukti pembayaran dari Agen46 tersebut</li>
                                                                        <li>Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</li>
                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card card-payment mt-1 mb-1">
                                                        <div class="card-header" id="headingTwoATMBersama">
                                                            <h5 class="btn btn-link" type="button"
                                                                data-toggle="collapse"
                                                                data-target="#collapseTwoATMBersama"
                                                                aria-expanded="true"
                                                                aria-controls="collapseTwoATMBersama">
                                                                ATM Bersama
                                                            </h5>
                                                        </div>
                                                        <div id="collapseTwoATMBersama" class="collapse"
                                                             aria-labelledby="headingTwoATMBersama"
                                                             data-parent="#accordionMenu">
                                                            <div class="card-body">
                                                                <div class="col-12">
                                                                    <ol>
                                                                        <li>Masukkan kartu ke mesin ATM Bersama</li>
                                                                        <li>Pilih Transaksi Lainnya</li>
                                                                        <li>Pilih menu "Transfer"</li>
                                                                        <li>Masukkan kode Bank BNI (009) dan nomor Virtual Account yang akan dituju
                                                                            <span class="font-weight-bold">880881861856</span>
                                                                        </li>
                                                                        <li>Masukkan nominal transfer sesuai tagihan atau kewajiban Anda. Nominal yang berbeda tidak dapat diproses</li>
                                                                        <li>Konfirmasi rincian Anda akan tampil di layar, cek dan tekan "Ya" untuk melanjutkan</li>
                                                                        <li>Transaksi berhasil</li>
                                                                        <li>Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</li>
                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card card-payment mt-1 mb-1">
                                                        <div class="card-header" id="headingTwoATMBankLain">
                                                            <h5 class="btn btn-link" type="button"
                                                                data-toggle="collapse"
                                                                data-target="#collapseTwoATMBanklain"
                                                                aria-expanded="true"
                                                                aria-controls="collapseTwoATMBanklain">
                                                                Bank Lain
                                                            </h5>
                                                        </div>
                                                        <div id="collapseTwoATMBanklain" class="collapse"
                                                             aria-labelledby="headingTwoATMBankLain"
                                                             data-parent="#accordionMenu">
                                                            <div class="card-body">
                                                                <div class="col-12">
                                                                    <ol>
                                                                        <li>Pilih menu "Transfer Antar Bank" atau "Transfer Online Antar Bank"</li>
                                                                        <li>Masukkan kode Bank BNI (009) atau pilih bank yang dituju yaitu BNI</li>
                                                                        <li>Masukkan nomor Virtual Account yang akan dituju
                                                                            <span class="font-weight-bold">880881861856</span> pada kolom rekening tujuan
                                                                        </li>
                                                                        <li>Masukkan nominal transfer sesuai tagihan atau kewajiban Anda. Nominal yang berbeda tidak dapat diproses</li>
                                                                        <li>Konfirmasi rincian Anda akan tampil di layar, cek dan apabila sudah sesuai silahkan lanjutkan transaksi sampai dengan selesai</li>
                                                                        <li>Transaksi berhasil</li>
                                                                        <li>Setelah transaksi pembayaran Anda selesai, invoice ini akan diperbarui secara otomatis. Ini bisa memakan waktu hingga 5 menit.</li>
                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card mt-1 mb-1">
                                    <div class="card-header" id="headingOnePERMATA">
                                        <h5 class="btn btn-link bold" type="button" data-toggle="collapse"
                                            data-target="#collapseOnePERMATA"
                                            aria-expanded="true"
                                            aria-controls="collapseOnePERMATA">
                                            PERMATA
                                        </h5>
                                    </div>
                                    <div id="collapseOnePERMATA" class="collapse"
                                         aria-labelledby="headingOnePERMATA"
                                         data-parent="#accordionExample">
                                        <div class="card-body bank-list">
                                            <div class="col-12 text-center virtual-account can-copy">
                                                <h3>Virtual Account # <span
                                                        class="data-copied">821454906526</span>
                                                </h3>
                                                <h3>Nama Virtual Akun
                                                    # GOMODO</h3>
                                            </div>
                                            <div class="col-12">
                                                <div class="accordion" id="accordionMenuPermata">
                                                    <div class="card card-payment mt-1 mb-1">
                                                        <div class="card-header" id="headingTwoATMPERMATA">
                                                            <h5 class="btn btn-link" type="button"
                                                                data-toggle="collapse"
                                                                data-target="#collapseTwoATMPERMATA"
                                                                aria-expanded="true"
                                                                aria-controls="collapseTwoATMPERMATA">
                                                                ATM ALTO
                                                            </h5>
                                                        </div>
                                                        <div id="collapseTwoATMPERMATA" class="collapse"
                                                             aria-labelledby="headingTwoATMPERMATA"
                                                             data-parent="#accordionMenuPermata">
                                                            <div class="card-body">
                                                                <div class="col-12">
                                                                    <ol>
                                                                        <li>
                                                                            Masukkan kartu, pilih bahasa kemudian masukkan PIN Anda
                                                                        </li>
                                                                        <li>
                                                                            Pada menu utama, pilih "Transaksi Lainnya"
                                                                        </li>
                                                                        <li>
                                                                            Pilih "Transfer Pembayaran"
                                                                        </li>
                                                                        <li>
                                                                            Pilih "Lain-lain"
                                                                        </li>
                                                                        <li>
                                                                            Pilih "Pembayaran Virtual Account"
                                                                        </li>
                                                                        <li>
                                                                            Masukkan nomor Virtual Account <b>821454906526</b>, lalu tekan "Benar"
                                                                        </li>
                                                                        <li>
                                                                            Pada halaman konfirmasi akan muncul jumlah yang akan dibayar, nomor rekening, dan nama merchant. Jika informasi telah sesuai, tekan "Benar"
                                                                        </li>
                                                                        <li>
                                                                            Pilih rekening pembayaran Anda, lalu tekan "Benar"
                                                                        </li>
                                                                        <li>
                                                                            Pembayaran Anda telah selesai. Simpan struk sebagai bukti pembayaran Anda.
                                                                        </li>
                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card card-payment mt-1 mb-1">
                                                        <div class="card-header"
                                                             id="headingTwoMobilebankingPERMATA">
                                                            <h5 class="btn btn-link" type="button"
                                                                data-toggle="collapse"
                                                                data-target="#collapseTwoMobileBankingPERMATA"
                                                                aria-expanded="true"
                                                                aria-controls="collapseTwoMobileBankingPERMATA">
                                                                Internet Banking
                                                            </h5>
                                                        </div>
                                                        <div id="collapseTwoMobileBankingPERMATA" class="collapse"
                                                             aria-labelledby="headingTwoMobilebankingPERMATA"
                                                             data-parent="#accordionMenuPermata">
                                                            <div class="card-body">
                                                                <div class="col-12">
                                                                    <ol>
                                                                        <li>
                                                                            Buka <a href="https://new.permatanet.com" target="_blank">https://new.permatanet.com</a> dan masuk ke akun Anda
                                                                        </li>
                                                                        <li>
                                                                            Pilih menu "Pembayaran" lalu "Pembayaran Tagihan"
                                                                        </li>
                                                                        <li>
                                                                            Pilih menu "Virtual Account" dan pilih rekening asal Anda
                                                                        </li>
                                                                        <li>
                                                                            Pilih "Masukkan Daftar Tagihan Baru"
                                                                        </li>
                                                                        <li>
                                                                            Masukkan nomor Virtual Account yang akan dituju <b>821454906526</b>
                                                                        </li>
                                                                        <li>
                                                                            Cek detail transaksi dan lanjutkan
                                                                        </li>
                                                                        <li>
                                                                            Masukkan kode respon SMS token
                                                                        </li>
                                                                        <li>
                                                                            Pembayaran Anda selesai.
                                                                        </li>
                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card card-payment mt-1 mb-1">
                                                        <div class="card-header"
                                                             id="headingTwoIbankPersonalPERMATA">
                                                            <h5 class="btn btn-link" type="button"
                                                                data-toggle="collapse"
                                                                data-target="#collapseTwoIbankPersonalPERMATA"
                                                                aria-expanded="true"
                                                                aria-controls="collapseTwoIbankPersonalPERMATA">
                                                                Permata Mobile
                                                            </h5>
                                                        </div>
                                                        <div id="collapseTwoIbankPersonalPERMATA"
                                                             class="collapse"
                                                             aria-labelledby="headingTwoIbankPersonalPERMATA"
                                                             data-parent="#accordionMenuPermata">
                                                            <div class="card-body">
                                                                <div class="col-12">
                                                                    <ol>
                                                                        <li>
                                                                            Buka Permata Mobile dan masuk ke akun Anda
                                                                        </li>
                                                                        <li>
                                                                            Pilih menu "Pembayaran Tagihan" lalu "Virtual Account"
                                                                        </li>
                                                                        <li>
                                                                            Pilih rekening asal Anda
                                                                        </li>
                                                                        <li>
                                                                            Pilih "Daftar Tagihan Baru"
                                                                        </li>
                                                                        <li>
                                                                            Masukkan nomor Virtual Account yang akan dituju <b>821454906526</b>
                                                                        </li>
                                                                        <li>
                                                                            Cek nama pelanggan dan lanjutkan
                                                                        </li>
                                                                        <li>
                                                                            Konfirmasi transaksi Anda
                                                                        </li>
                                                                        <li>
                                                                            Masukkan kode respon SMS token
                                                                        </li>
                                                                        <li>
                                                                            Pembayaran Anda selesai
                                                                        </li>
                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card card-payment mt-1 mb-1">
                                                        <div class="card-header"
                                                             id="headingThreeIbankPersonalPERMATA">
                                                            <h5 class="btn btn-link" type="button"
                                                                data-toggle="collapse"
                                                                data-target="#collapseThreeIbankPersonalPERMATA"
                                                                aria-expanded="true"
                                                                aria-controls="collapseThreeIbankPersonalPERMATA">
                                                                Permata Mobile X
                                                            </h5>
                                                        </div>
                                                        <div id="collapseThreeIbankPersonalPERMATA"
                                                             class="collapse"
                                                             aria-labelledby="headingThreeIbankPersonalPERMATA"
                                                             data-parent="#accordionMenuPermata">
                                                            <div class="card-body">
                                                                <div class="col-12">
                                                                    <ol>
                                                                        <li>
                                                                            Buka aplikasi Permata Mobile X dan masuk ke akun Anda
                                                                        </li>
                                                                        <li>
                                                                            Pilih menu "Pay Bills" atau "Pembayaran Tagihan"
                                                                        </li>
                                                                        <li>
                                                                            Pilih menu "Virtual Account"
                                                                        </li>
                                                                        <li>
                                                                            Masukkan nomor Virtual Account yang akan dituju <b>821454906526</b>
                                                                        </li>
                                                                        <li>
                                                                            Detail tagihan akan muncul di layar lalu pilih akun sumber dana pembayaran Anda
                                                                        </li>
                                                                        <li>
                                                                            Konfirmasi transaksi Anda
                                                                        </li>
                                                                        <li>
                                                                            Masukkan kode respon SMS token
                                                                        </li>
                                                                        <li>
                                                                            Pembayaran Anda telah selesai
                                                                        </li>
                                                                    </ol>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<footer class="container-fluid bg-dark">
    <div class="container pt-5">
        <div class="row">
            <div class="col-md-4 text-white mb-md-0 mb-3 text-justify">
                <p class="text-white about">
                    Tentang kami
                </p>
                <p class="text-white order-0 about-us">
                <p><?php echo $getProvider['tentang'];?></p>
                </p>
            </div>
            <div class="col-md-4 order-1 order-lg-1 d-flex">
                <div class="address">
                    <p id="address"> Alamat</p>
                    <p><?php echo $getProvider['alamat'];?></p>

                    <div class="phone-company">
                        <span class="text-white">
                            <img src="assets/048-telephone-1.png" width="16" alt="">
                            <span><?php echo $getProvider['nohp'];?></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-md-4 order-1 order-lg-2 mb-md-0 mb-5">
                <ul class="social">
                    <p>Media Sosial</p>
                    <li>
                        <a href="#" class="text-white font-weight-normal">
                            <img src="assets/159-email.png" alt=""> <?php echo $getProvider['email'];?>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="text-white font-weight-normal">
                            <img src="assets/099-facebook.png" alt=""> <?php echo $getProvider['fb'];?>
                        </a>
                    </li>
                    <li>
                        <a href="#" class="text-white font-weight-normal">
                            <img src="assets/080-instagram.png" alt=""> <?php echo $getProvider['ig'];?>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 text-center mt-3">
                <div class="powered">
                    <span>POWERED BY</span> <span class="bold"><a href="home.php">GOMODO</a></span>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="http://majumuju.kadalmacho.top/landing/js/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>


<script type="text/javascript" src="http://majumuju.kadalmacho.top/js/public/material.js"></script>

<script>
    $(document).on('click', '#btn-navbar-toggle', function () {
        let t = $(this);
        $(t.data('target')).slideToggle(100);
    });

    function loadingStart() {
        $('.loading').addClass('show');
    }

    function loadingFinish() {
        $('.loading').removeClass('show');
    }

    toastr.options = {
        "positionClass": "toast-bottom-right",
    };
    // $(document).on('click', '.current-language', function () {
    //     $('.language-option').slideToggle(300)
    // });
    // $(document).on('click', '.box-language', function () {
    //     let form = $('#landing-change-language');
    //     form.find('input[name=lang]').val($(this).parent().data('value'));
    //     form.submit();
    // })
    $(document).on('click', '.can-copy', function () {
        let t = $(this);
        let $temp = $("<input>");
        $("body").append($temp);
        $temp.val(t.find('.data-copied').text()).select();
        document.execCommand("copy");
        $temp.remove();
        toastr.success(t.find('.data-copied').text(), 'Copied');
    })
    if (typeof gomodo !== 'undefined') {
        if (gomodo.error !== undefined) {
            toastr.error(gomodo.error, 'Terjadi kesalahan');
        }
        if (gomodo.success !== undefined) {
            toastr.success(gomodo.success);
        }
        if (gomodo.warning !== undefined) {
            toastr.warning(gomodo.warning);
        }
        if (gomodo.info !== undefined) {
            toastr.info(gomodo.info);
        }
    }
    // Prevent Char in Input Type Number
    $('input[type="number"]').on('change keydown', function onChange(e) {
        if (e.metaKey == false) { // Enable metakey
            if (e.keyCode > 13 && e.keyCode < 48 && e.keyCode != 39 && e.keyCode != 37 || e.keyCode > 57) {
                e.preventDefault(); // Disable char. Enable arrow
            }
            ;
            if (e.shiftKey === true) { // Disable symbols
                if (e.keyCode > 46 && e.keyCode < 65) {
                    e.preventDefault();
                }
            }
        }
    })
    // disable mousewheel on a input number field when in focus
    // (to prevent Cromium browsers change the value when scrolling)
    $('form').on('focus', 'input[type=number]', function (e) {
        $(this).on('mousewheel.disableScroll', function (e) {
            e.preventDefault()
        })
    })
    $('form').on('blur', 'input[type=number]', function (e) {
        $(this).off('mousewheel.disableScroll')
    })

</script>
<script>
    // Set the date we're counting down to
    var countDownDate = new Date().getTime();
    countDownDate = countDownDate + (1000*60*60*24);
    console.log(countDownDate);

    // Update the count down every 1 second
    var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Output the result in an element with id="demo"

        document.getElementsByClassName("hours")[0].innerHTML = hours;
        document.getElementsByClassName("minutes")[0].innerHTML = minutes;
        document.getElementsByClassName("seconds")[0].innerHTML = seconds;
        $('.minutes').html(minutes);
        document.getElementsByClassName("minutes").innerHTML = minutes;
        document.getElementsByClassName("seconds").innerHTML = seconds;

        // document.getElementById("demo").innerHTML = hours + "h "
        //     + minutes + "m " + seconds + "s ";

        // If the count down is over, write some text
        if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo").innerHTML = "EXPIRED";
        }
    }, 1000);
</script>
</body>
</html>
